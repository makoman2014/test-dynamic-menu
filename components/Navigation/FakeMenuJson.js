export const MenuItems = [
    {
        name: 'First Level',
        id:1,
        children:[
                {
                    name: 'Second Level',
                    id: 2,
                    children:[]
                },
                {

                    name: 'Second Level 2',
                    id: 3,
                    children:[
                        {

                            name: 'Second Level 3',
                            id: 4,
                            children:[]

                        }
                    ]
                },
                {

                    name: 'Second Level 2',
                    id: 5,
                    children:[
                        {

                            name: 'Second Level 3',
                            id: 6,
                            children:[
                                {
                                    name: 'Second Level',
                                    id: 7,
                                    children:[]
                                },
                            ]

                        }
                    ]
                },
            ]

    },
    {
        name: 'First Level',
        id:11,
        children:[
            {
                name: 'Second Level',
                id: 12,
                children:[]
            },
            {

                name: 'Second Level 2',
                id: 13,
                children:[
                    {

                        name: 'Second Level 3',
                        id: 14,
                        children:[]

                    }
                ]
            },
            {

                name: 'Second Level 2',
                id: 15,
                children:[
                    {

                        name: 'Second Level 3',
                        id: 16,
                        children:[
                            {
                                name: 'Second Level',
                                id: 17,
                                children:[]
                            },
                        ]

                    }
                ]
            },
        ]

    }

];