import React from "react";
import {NavigationElement} from "./NavigationElement";
import {MenuItems} from "./FakeMenuJson";

export function NavigationPanel() {
    return (
        <>
            <nav>
                <NavigationElement menuItems={MenuItems}/>
            </nav>
        </>
    )
}