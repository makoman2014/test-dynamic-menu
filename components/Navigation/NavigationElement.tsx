import React, {useState} from "react";
import {MenuInterface} from "../../interfaces/menuInterface";

interface menuOpenInterFace {
    [key: string]: number;
}

export const NavigationElement = ({menuItems}:MenuInterface) => {
    const [openMenu, setOpenMenu] = useState<menuOpenInterFace>({})
    return (
        <ul>
            {menuItems && menuItems.map(menu=>
                <li key={menu.id}>
                    <div className="menu-row">
                        {menu?.children?.length > 0 ?
                            <span
                                onClick={():void => setOpenMenu({...openMenu, [menu.id]: !openMenu[menu.id]})}
                                className={`arrow_icon ${openMenu[menu.id] ? 'active' : ''}`}
                            >
                                <svg width="16" height="24" viewBox="0 0 16 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path opacity="0.48" d="M3.41 8L8 12.59L12.59 8L14 9.42L8 15.42L2 9.42L3.41 8Z" fill="#78909C"/>
                                </svg>
                            </span>
                            :
                            <span className="arrow_icon"></span>
                        }
                        <span className="menu-image">
                        {menu?.children?.length ?
                            <svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M8 0H2C0.89 0 0 0.89 0 2V14C0 14.5304 0.210714 15.0391 0.585786 15.4142C0.960859 15.7893 1.46957 16 2 16H18C18.5304 16 19.0391 15.7893 19.4142 15.4142C19.7893 15.0391 20 14.5304 20 14V4C20 2.89 19.1 2 18 2H10L8 0Z" fill="#78909C"/>
                            </svg>
                            :
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M13 9V3.5L18.5 9H13ZM6 2C4.89 2 4 2.89 4 4V20C4 20.5304 4.21071 21.0391 4.58579 21.4142C4.96086 21.7893 5.46957 22 6 22H18C18.5304 22 19.0391 21.7893 19.4142 21.4142C19.7893 21.0391 20 20.5304 20 20V8L14 2H6Z" fill="#78909C"/>
                            </svg>
                        }
                     </span>
                        <span className="menu_name">{menu.name}</span>
                    </div>
                    {menu?.children?.length > 0 && openMenu[menu.id] && <NavigationElement  menuItems={menu.children} />}
                </li>
            )}
        </ul>
    )
}