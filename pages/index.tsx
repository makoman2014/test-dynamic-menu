import React from 'react'
import Head from 'next/head'
import {NavigationPanel} from '../components/Navigation/NavigationPanel'


export default function Index() {

    return (
        <>
            <Head>
                <title>Navigation</title>
            </Head>
            <NavigationPanel />
        </>
    )
}

