export interface MenuInterface {
  menuItems:
    {
      name: string;
      id: number
      children?: any;
    }[]

}
